const express = require('express');
const app = express();

const cors = require('cors');
const bodyParser = require('body-parser');

// DB TEST
const mongo = require('./db');
const articleModel = require('./model');

// EXTRAS
const fetch = require('node-fetch');
const url = 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs';
const settings = { method: 'Get' };
const cron = require('node-cron');

// SETTINGS
app.set('port', process.env.PORT || 4000);

// MIDDLEWARES
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());

// LOAD DATA FROM API
cron.schedule('0 * * * *', () => {
	console.log('Refreshing Data At ' + new Date());
	// FETCH ALGOLIA'S URL
	fetch(url, settings)
		.then((res) => res.json())
		.then(async (data) => {
			// FETCH COLLECTION
			for (const item of data.hits) {
				articleModel.findById({ api_id: item.objectID }, (err, result) => {
					if (result && item.status !== 'deleted') {
						const newItem = new articleModel({
							api_id: item.objectID,
							title: item.story_title || item.title,
							author: item.author,
							created_at: item.created_at,
							url: item.story_url || item.url,
							status: 'active',
						});
						newItem.save();
					}
				});
			}
		});
});

// ROUTES
app.get('/fetchData', async function (req, res) {
	// RETRIEVE DATA
	let data = await articleModel.find({ status: 'active' });
	res.json(data);
});

app.put('/:id', async function (req, res) {
	// DELETE ARTICLE FROM COLLECTION
	let id = req.params.id;
	articleModel.findByIdAndUpdate({ _id: id }, { status: 'deleted' }, (err, result) => {
		if (err) return res.status(500).send(err);
		const response = {
			message: `${id} deleted`,
		};
		res.json(response);
	});
});

// START SERVER
app.listen(app.get('port'), () => {
	// FETCH ALGOLIA'S URL
	fetch(url, settings)
		.then((res) => res.json())
		.then((data) => {
			// DELETE COLLECTION IF EXISTS
			mongo.connection.db.listCollections({ name: 'articles' }).next((err, result) => {
				if (result) {
					mongo.connection.db.dropCollection('articles');
				}
				// FETCH COLLECTION
				for (const item of data.hits) {
					const newItem = new articleModel({
						title: item.story_title || item.title,
						author: item.author,
						created_at: item.created_at,
						url: item.story_url || item.url,
						status: 'active',
					});
					newItem.save();
				}
				console.log('Data Fetched At ' + new Date());
			});
		});

	console.log('Server on port ' + app.get('port'));
});
