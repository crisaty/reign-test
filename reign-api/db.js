const mongoose = require('mongoose');

const URI = 'mongodb://mongo:27017/reign-db';
// const URI = 'mongodb://localhost:27017/reign-db';
mongoose
	.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true })
	.then(() => console.log('MongoDB Connected'))
	.catch((err) => console.error(err));

mongoose.set('useFindAndModify', false);

module.exports = mongoose;
