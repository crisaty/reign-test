const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const articleSchema = new Schema({
	api_id: {
		type: Number,
	},
	title: {
		type: String,
	},
	author: {
		type: String,
	},
	created_at: {
		type: Date,
	},
	url: {
		type: String,
	},
	status: {
		type: String,
	},
});

module.exports = Item = mongoose.model('article', articleSchema);
