import { Component, OnInit } from '@angular/core';

// EXTRAS
import { ApiService } from './api.service';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
	constructor(public apiService: ApiService) {}

	_data: any;

	// LOAD DATA
	ngOnInit(): void {
		this.fetchData();
	}

	// FETCH ARTICLES
	fetchData(): void {
		this.apiService.fetchData().subscribe((data) => {
			this._data = Object.values(data).sort(function (a, b) {
				return new Date(b.created_at).getTime() - new Date(a.created_at).getTime();
			});
		});
	}

	// DELETE ARTICLE
	deleteArticle(event, id): void {
		console.log('deleting ' + id);
		event.stopPropagation();
		this.apiService.deleteArticle(id).subscribe((data) => {
			console.log(data);
			this.fetchData();
		});
	}

	// OPEN ARTICLE LINK
	openLink(url): void {
		console.log(url);
		window.open(url, '_blank');
	}
}
