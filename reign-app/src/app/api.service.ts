import { Injectable } from '@angular/core';

// EXTRAS
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
	providedIn: 'root',
})
export class ApiService {
	constructor(private http: HttpClient) {}

	_data: any;

	// FETCH DATA
	public fetchData() {
		return this.http.get(`http://localhost:4000/fetchData`);
	}

	// DELETE ARTICLE
	public deleteArticle(id) {
		return this.http.put(`http://localhost:4000/${id}`, { status: 'deleted' });
	}
}
