# reign-test

To run this demo it is necessary install Docker in the system, then run the command in the project root folder.

`docker-compose up`

the data is loaded when the app is running and refreshing it every one hour.

* **Database:** reign-db - PORT: 27017
* **API (Server):** reign-api - PORT: 4000
* **APP (Client):** reign-app - PORT: 4200